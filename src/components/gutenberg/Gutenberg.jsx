import React from "react";
import {
  BlockEditorProvider,
  BlockList,
  WritingFlow,
  ObserveTyping
} from "@wordpress/block-editor";
import { registerCoreBlocks } from "@wordpress/block-library";
import { Popover } from "@wordpress/components";
import { useState } from "@wordpress/element";
import "./Gutenberg.css";

registerCoreBlocks();

function Gutenberg() {
  const [blocks, updateBlocks] = useState([]);

  return (
    <BlockEditorProvider
      value={blocks}
      onInput={updateBlocks}
      onChange={updateBlocks}
    >
      <WritingFlow>
        <ObserveTyping>
          <BlockList />
        </ObserveTyping>
      </WritingFlow>
      <Popover.Slot />
    </BlockEditorProvider>
  );
}

export default Gutenberg;
