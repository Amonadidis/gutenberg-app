import React from "react";
import Gutenberg from "../gutenberg/Gutenberg";

function App() {
  return <Gutenberg />;
}

export default App;
